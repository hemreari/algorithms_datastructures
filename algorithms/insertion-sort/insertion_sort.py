arr = [5, 6, 3, 4, 1, 0, 2]

for j in range(0, len(arr)):
    key = arr[j]
    i = j - 1
    while (i >= 0 and arr[i] > key):
        arr[i+1] = arr[i]
        i = i - 1
    arr[i+1] = key;

for i in range(0, len(arr)):
    print("arr[", i, "] --> ", arr[i], sep='');
