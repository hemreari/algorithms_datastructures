#include <stdio.h>

#define ARR_LENGTH(_array) (sizeof(_array) / sizeof(_array[0]))

int main()
{
  int j, i, key;
  int arr[] = {5, 6, 3, 4, 1, 0, 2};

  key = 3;
  j = 0;
  while (j < ARR_LENGTH(arr))
    {
      j++;
      if (arr[j] == key)
	break;
    }

  printf("The key that is searching is %d and its index is %d\n", key, j);
  
  return 0;
}
