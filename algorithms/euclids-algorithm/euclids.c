#include "euclids.h"
#include <stdio.h>

/* Euclid's Algorithm:                     *
 * This algorithm finds greatest positive  *
 * common integer between two integer and  *
 * prints this integer(s) to screen        */

void find_divisor(int num, int num1)
{
  int i ,remainder, remainder1;
  int div;

  printf("%d and %d of common divisor(s) is: \n", num, num1);

  i = 1;
  while (i <= num || i <= num1)
    {
      remainder = num % i;
      remainder1 = num1 % i; 
      if ((remainder == 0) && (remainder1 == 0))
	{
	  printf("%d\n", i);
	  i++;
	}
      else
	i++;
    }

}
