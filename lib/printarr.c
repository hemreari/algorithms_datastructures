#include "printarr.h"
#include <stdio.h>

void print_array(int arr[], int length)
{
  int i;

  for (i = 0; i <= length; i++)
    printf("arr[%d]  --> %d\n",i, arr[i] );
  printf("%0*d\n", 30, 0);
}
