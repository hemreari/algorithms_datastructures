#include <stdlib.h>
#include <string.h>

#include "list.h"

void list_init(List *list, void (*destroy)(void *data))
{
  list->size = 0;
  list->destory = destory;
  list->head = NULL;
  list->tail = NULL;

  return;
}

void list_destroy(List *list)
{
  void *data;

  while (list_size(list) > 0)
    {
      if (list_rem_next(list, NULL, (void **)&data) == 0 && list->destroy != NULL)
	{
	  list_destroy(data);
	}
    }

  /* no operations are allowed now, but clear the structure as a precaution. */

  memset(list, 0, sizeof(List));

  return 0;
}

int list_ins_next(List *list, ListElmt *element, const void *data)
{
  ListElmt *new_element;

  /* allocate storage for the element */
  if ((new_element = (ListElmt *)malloc(sizeof(ListElmt))) == NULL)
    return -1;

  /* insert the element into the list */
  new_element->data = (void *)data;

  if (element == NULL)
    {
      /* handle insertion at the head of the list */
      if(list_size(list) == 0)
	list_tail = new_element;

      new_element->next = list->head;
      list->head = new_element;
    }
  else
    {
      
    }
}
