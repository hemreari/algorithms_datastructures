/*
 * simplelist.c                        *
 * In this program, I illustrate simple *
 * linked list structure and defined a *
 * function that prints the elements   *
 * of a list.                          * 
*/

#include <stdio.h>
#include <stdlib.h>

struct Node
{
  int data;
  struct Node *next;
};

void printlist(struct Node *node);

int main()
{
  struct Node *head = NULL;
  struct Node *second = NULL;
  struct Node *third = NULL;

  /* allocate memory for nodes */
  head = (struct Node*)malloc(sizeof(struct Node));
  second = (struct Node*)malloc(sizeof(struct Node));
  third = (struct Node*)malloc(sizeof(struct Node));
  head->data = 1;
  head->next = second; /* link first node with the second node */

  second->data = 2;
  second->next = third; /* link second node with the third node */

  third->data = 3;
  third->next = NULL; /* next pointer of the third block is made NULL to indicate
		      * that the linked list is terminated here. */
  
  printlist(head);
  
  return 0;
}

void printlist(struct Node *node)
{
  while (node != NULL) {
      printf("element of the list is --> %d\n", node->data);
      node = node->next;
  }
}
